from torchvision import transforms
import albumentations as A
from PIL import Image
import cv2
import torch
import numpy as np

# channel means and standard deviations of kaggle dataset, computed by origin author
MEAN = [108.64628601 / 255, 75.86886597 / 255, 54.34005737 / 255]
STD = [70.53946096 / 255, 51.71475228 / 255, 43.03428563 / 255]

# for color augmentation, computed by origin author
U = torch.tensor([[-0.56543481, 0.71983482, 0.40240142],
                  [-0.5989477, -0.02304967, -0.80036049],
                  [-0.56694071, -0.6935729, 0.44423429]], dtype=torch.float)

EV = torch.tensor([1.65513492, 0.48450358, 0.1565086], dtype=torch.float)
# Applying Transforms to the Data

image_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(size=256, scale=(0.8, 1.0)),
        transforms.RandomRotation(degrees=15),
        transforms.RandomHorizontalFlip(),
        transforms.CenterCrop(size=224),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD)
    ]),
    'valid': transforms.Compose([
        transforms.Resize(size=256),
        transforms.CenterCrop(size=224),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD)
    ]),
    'test': transforms.Compose([
        transforms.Resize(size=256),
        transforms.CenterCrop(size=224),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD)
    ])
}

cv2_transforms = {
    'train': A.Compose([
        A.Resize(256, 256, interpolation=cv2.INTER_LINEAR),
        A.CenterCrop(224, 224),
        A.HorizontalFlip(p=0.5),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD)
    ])
}

pil_transform = {
    'train': transforms.Compose([
        transforms.Resize((256, 256), interpolation=2),
        transforms.CenterCrop(size=224),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD)
    ])
}

class KrizhevskyColorAugmentation(object):
    def __init__(self, EV, U, sigma=0.5):
        self.EV = EV
        self.U = U
        self.sigma = sigma
        self.mean = torch.FloatTensor([0.0])
        self.deviation = torch.FloatTensor([sigma])

    def __call__(self, img):
        sigma = self.sigma
        if not sigma > 0.0:
            color_vec = torch.zeros(3, dtype=torch.float32)
        else:
            color_vec = torch.distributions.Normal(self.mean, self.deviation).sample((3,))

        color_vec = color_vec.squeeze()
        alpha = color_vec * self.EV
        noise = torch.matmul(self.U, alpha.t())
        noise = noise.view((3, 1, 1))
        return img + noise

    def __repr__(self):
        return self.__class__.__name__ + '(sigma={})'.format(self.sigma)


def data_transforms(mode = 'random',img_size = 256):
    general_aug = A.Compose([
        A.OneOf([
            A.Transpose(),
            A.HorizontalFlip(),
            A.RandomRotate90()
        ]),
        A.ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.2, rotate_limit=15, p=.2),
        A.OneOf([
            A.OpticalDistortion(p=0.2),
            A.GridDistortion(distort_limit=0.2, p=.1),
            A.ElasticTransform(),
            ], p=1.)
        ], p=1)

    image_specific = A.Compose([
        A.OneOf([
            A.IAASharpen(),
            A.RandomContrast(),
            A.RandomBrightness(),
            ], p=0.3)
        ])

    all_trans_pre = [transforms.RandomCrop(round(1.2 * img_size))]

    all_trans_after = [transforms.CenterCrop(img_size)]
    center_crop = [transforms.CenterCrop(img_size)]
    normalize = [transforms.ToTensor()]

    def get_augment(aug):
        def augment(image):
            return Image.fromarray(aug(image=np.array(image))['image'])
        return [augment]

    def normalize_to_full_image(img):
        return img
        #img = np.array(img).astype(np.float32)
        #img -= img.min()
        #img /= img.max()
        #img *= 255
        #return img.astype(np.uint8)

    pre_crop = transforms.Compose(all_trans_pre)
    train_img_transform = transforms.Compose(get_augment(general_aug) + get_augment(image_specific) + [normalize_to_full_image])
    norm_transform = transforms.Compose(all_trans_after + normalize)
    val_transform = transforms.Compose(all_trans_after) if mode == 'random' else transforms.Compose(center_crop)

    return pre_crop, train_img_transform, norm_transform, val_transform


def transform_generate(image_size, scale, stretch_ratio, rotation, translation_ratio, sigma):
    """
    :param image_size:
    :param scale:
    :param stretch_ratio:
    :param rotation:
    :param translation_ratio:
    :param sigma:
    :return:
    """
    train_transform = transforms.Compose([
        transforms.RandomResizedCrop(
            size=image_size,
            scale=scale,
            ratio=stretch_ratio
        ),
        transforms.RandomAffine(
            degrees=rotation,
            translate=translation_ratio,
            scale=None,
            shear=None
        ),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(MEAN, STD),
        KrizhevskyColorAugmentation(EV=EV, U=U, sigma=sigma)
    ])

    test_transform = transforms.Compose([
        transforms.Resize(image_size),
        transforms.ToTensor(),
        transforms.Normalize(tuple(MEAN), tuple(STD))
    ])

    image_transforms = {
        'train': train_transform,
        'test': test_transform
    }

    return image_transforms # Image.open(x)
