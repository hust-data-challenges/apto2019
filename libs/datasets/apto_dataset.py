import datetime
import math
import time

import numpy as np
import torch
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from torch.utils.data import Dataset, Sampler
from libs.utils.common import cv2_imread, pil_imread
import os
import pandas as pd
from libs.utils.common import kfold_split, read_confusion_set, category_to_tensor, category_to_one_hot_tensor

# s
# et of resampling weights that yields balanced classes, computed by origin author
BALANCE_WEIGHTS = torch.DoubleTensor([1.3609453700116234, 14.378223495702006,
                                6.637566137566138, 40.235967926689575,
                                49.612994350282484])

class TrainDataProvider:
    def __init__(self, data_dir, test_size, fold, confusion_set, train_on_val=False):
        start_time = time.time()
        df = pd.read_csv(data_dir + 'train.csv')
        df['path'] = df['id_code'].apply(lambda code: os.path.join(data_dir, 'train_images', code))
        print("Loaded {} samples".format(len(df)))
        image_paths = df['path'].values.tolist()
        categories = df['diagnosis'].values.tolist()

        if fold is None:
            train_categories, val_categories, train_images, val_images = \
                train_test_split(
                    categories,
                    image_paths,
                    test_size=test_size,
                    stratify=categories,
                    random_state=42
                )
        else:
            train_indexes, val_indexes = list(kfold_split(3, range(len(categories)), categories))[fold]

            train_categories = [categories[i] for i in train_indexes]
            train_images = [image_paths[i] for i in train_indexes]

            val_categories = [categories[i] for i in val_indexes]
            val_images = [image_paths[i] for i in val_indexes]

        if train_on_val:
            train_categories = categories
            train_images = image_paths

        if confusion_set is not None:
            confusion_set_categories = read_confusion_set(
                "/storage/models/seresnext50_confusion/confusion_set_{}.txt".format(confusion_set))

            categories_mask = np.array([c in confusion_set_categories for c in categories])

            train_category_filter = np.array([categories_mask[dc] for dc in train_categories])
            train_categories = train_categories[train_category_filter]
            train_images = train_images[train_category_filter]

            val_category_filter = np.array([categories_mask[dc] for dc in val_categories])
            val_categories = val_categories[val_category_filter]
            val_images = val_images[val_category_filter]

            category_mapping = {}
            for csc in confusion_set_categories:
                category_mapping[categories.index(csc)] = confusion_set_categories.index(csc)
            train_categories = np.array([category_mapping[c] for c in train_categories])
            val_categories = np.array([category_mapping[c] for c in val_categories])
            categories = confusion_set_categories

        self.train_set_df = {
            "category": train_categories,
            "image": train_images,
        }
        self.val_set_df = {
            "category": val_categories,
            "image": val_images,
        }
        self.categories = categories

        end_time = time.time()
        print("Time to load data : {}".format(str(datetime.timedelta(seconds=end_time - start_time))),
              flush=True)


class APTODataset(Dataset):
    def __init__(self, df, image_size, transform):
        super().__init__()
        self.categories = [0, 1, 2, 3, 4]
        self.num_categories = len(self.categories)
        self.image_paths = df['image']
        self.categories_data = df['category']
        self.image_size = image_size
        self.transform = transform

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, index):
        path = self.image_paths[index] + '.png'
        category = self.categories_data[index]
        image = pil_imread(path)

        if self.transform:
            image = self.transform(image)
        category = category_to_tensor(category)
        category_one_hot = category_to_one_hot_tensor(category, self.num_categories)

        # values_channel = calculate_images_values_channel(images, country, self.image_size)
        # image = torch.cat([torch.from_numpy(values_channel).float().unsqueeze(0), image], dim=0)

        # image = normalize(image, (0.485, 0.456, 0.406), (0.229, 0.224, 0.225))

        return image, category


class StratifiedSampler(Sampler):
    def __init__(self, class_vector, batch_size):
        super().__init__(None)
        self.class_vector = class_vector
        self.batch_size = batch_size

    def gen_sample_array(self):
        n_splits = math.ceil(len(self.class_vector) / self.batch_size)
        splitter = StratifiedShuffleSplit(n_splits=n_splits, test_size=0.5)
        train_index, test_index = next(splitter.split(np.zeros(len(self.class_vector)), self.class_vector))
        return np.hstack([train_index, test_index])

    def __iter__(self):
        return iter(self.gen_sample_array())

    def __len__(self):
        return len(self.class_vector)


class ScheduledWeightedSampler(Sampler):
    def __init__(self, num_samples, train_targets, decay_rate, replacement=True, balance_weights=BALANCE_WEIGHTS):
        super().__init__(None)
        self.num_samples = num_samples
        self.train_targets = train_targets
        self.decay_rate = decay_rate
        self.replacement = replacement

        self.epoch = 0
        self.w0 = balance_weights
        self.wf = torch.as_tensor([1, 2, 2, 2, 2], dtype=torch.double)
        self.train_sample_weight = torch.zeros(len(train_targets), dtype=torch.double)

    def step(self):
        self.epoch += 1
        factor = self.decay_rate ** (self.epoch - 1)
        self.weights = factor * self.w0 + (1 - factor) * self.wf
        for i, _class in enumerate(self.train_targets):
            self.train_sample_weight[i] = self.weights[_class]

    def __iter__(self):
        return iter(torch.multinomial(self.train_sample_weight, self.num_samples, self.replacement).tolist())

    def __len__(self):
        return self.num_samples
