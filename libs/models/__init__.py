from .senet_wrapper import SeNet, SeResNext50Cs
from .resnet import resnet50 as ResNet50
from .stacknet import StackNet
from .ensemble import Ensemble
from .o_onet import o_ONet
from .oridinal_logistic import OrdinalLogisticModel
import torch.nn as nn


def create_backbone(type, num_classes, net_size=None):
    if type == "resnet":
        model = ResNet50(num_classes=num_classes)
    elif type in ["seresnext50", "seresnext101", "seresnet50", "seresnet101", "seresnet152", "senet154"]:
        model = SeNet(type=type, num_classes=num_classes)
    elif type == "seresnext50_cs":
        model = SeResNext50Cs(num_classes=num_classes)
    elif type == "res_o_O":
        model = o_ONet(net_size=net_size)
    elif type == "stack":
        model = StackNet(num_classes=num_classes)
    else:
        raise Exception("Unsupported model type: '{}".format(type))

    # return nn.DataParallel(model)
    return model


def create_model(backbone_type, num_classes, infer_type=None,
                 init_cutpoints: str = 'ordered', net_size=None):
    model = Final_Model(backbone_type, num_classes, infer_type, init_cutpoints, net_size)
    # return nn.DataParallel(model)
    return model


def create_infer_model(type, model, num_classes, init_cutpoints):
    if type == 'ordinal_log':
        model = OrdinalLogisticModel(model, num_classes, init_cutpoints)
    return model


class Final_Model(nn.Module):
    def __init__(self, backbone_type, num_classes, infer_type=None, init_cutpoints: str = 'ordered',
                            net_size=None):
        super().__init__()
        dim_fc_features = num_classes
        if infer_type:
            dim_fc_features = 1
        self.dim_fc_features = dim_fc_features
        self.backbone = create_backbone(backbone_type, dim_fc_features, net_size)
        if self.dim_fc_features == 1:
            self.infer_model = create_infer_model(infer_type, self.backbone,
                                                  num_classes, init_cutpoints)

    def features(self, x):
        x = self.backbone(x)
        return x

    def forward(self, x):
        if self.dim_fc_features == 1:
            x = self.infer_model(x)
        else:
            x = self.features(x)
        return x