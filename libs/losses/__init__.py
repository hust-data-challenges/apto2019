from .focal_loss import FocalLoss
from torch.nn import MSELoss
from .l2_loss import regularize_loss