import torch
import torch.nn.functional as F
import numpy as np
from libs.utils.common import zero_item_tensor
from tqdm import tqdm


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# metrics
def classify(predict, thresholds=[0, 0.5, 1.5, 2.5, 3.5]):
    predict = max(predict, thresholds[0])
    for i in reversed(range(len(thresholds))):
        if predict >= thresholds[i]:
            return i


def quadratic_weighted_kappa(conf_mat):
    assert conf_mat.shape[0] == conf_mat.shape[1]
    cate_num = conf_mat.shape[0]

    # Quadratic weighted matrix
    weighted_matrix = np.zeros((cate_num, cate_num))
    for i in range(cate_num):
        for j in range(cate_num):
            weighted_matrix[i][j] = 1 - float(((i - j)**2) / ((cate_num - 1)**2))

    # Expected matrix
    ground_truth_count = np.sum(conf_mat, axis=1)
    pred_count = np.sum(conf_mat, axis=0)
    expected_matrix = np.outer(ground_truth_count, pred_count)

    # Normalization
    conf_mat = conf_mat / conf_mat.sum()
    expected_matrix = expected_matrix / expected_matrix.sum()

    observed = (conf_mat * weighted_matrix).sum()
    expected = (expected_matrix * weighted_matrix).sum()
    return (observed - expected) / (1 - expected)


def regre_evaluate(model_path, test_data_loader):
    c_matrix = np.zeros((5, 5), dtype=int)

    trained_model = torch.load(model_path).to(device)
    # test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)
    test_acc = _eval(trained_model, test_data_loader, c_matrix)
    print('========================================')
    print('Finished! test acc: {}'.format(test_acc))
    print('Confusion Matrix:')
    print(c_matrix)
    print('quadratic kappa: {}'.format(quadratic_weighted_kappa(c_matrix)))
    print('========================================')


def regre_accuracy(predictions, targets, c_matrix=None):
    predictions = predictions.data
    targets = targets.data

    # avoid modifying origin predictions
    predicted = torch.FloatTensor(
        [classify(p.item()) for p in predictions]
    ).to(device)

    # update confusion matrix
    if c_matrix is not None:
        for i, p in enumerate(predicted):
            c_matrix[int(targets[i])][int(p.item())] += 1

    correct = (predicted == targets).sum().item()
    return correct / len(predicted)

# def evaluate(model, test_data_loader, criterion, topk=1):
#     model.eval()
#
#     loss_sum_t = zero_item_tensor()
#     mapk_sum_t = zero_item_tensor()
#     accuracy_top1_sum_t = zero_item_tensor()
#     step_count = 0
#     progress = tqdm(enumerate(test_data_loader))
#     with torch.no_grad():
#         for batch in progress:
#             X, y = batch
#             X, y = X.to(device), y.float().to(device)
#
#             y_pred = model(X)
#             loss = criterion(y_pred, y)
#

def accuracy(prediction_logits, categories, topk=1):
    predictions = F.softmax(prediction_logits, dim=1)
    _, predicted_categories = predictions.topk(topk, dim=1, sorted=True)
    apk_v = torch.eq(categories, predicted_categories[:, 0]).float()
    for k in range(1, topk):
        apk_v += torch.eq(categories, predicted_categories[:, k]).float()

    return apk_v.mean()
#=========================

def _eval(model, dataloader, c_matrix=None):
    model.eval()
    torch.set_grad_enabled(False)

    correct = 0
    total = 0
    for test_data in dataloader:
        X, y = test_data
        X, y = X.to(device), y.float().to(device)

        y_pred = model(X)
        total += y.size(0)
        correct += accuracy(y_pred, y, c_matrix) * y.size(0)
    acc = round(correct / total, 4)

    model.train()
    torch.set_grad_enabled(True)
    return acc

def evaluate(epoch, model , dataloader, criterion, mapk_topk):
    model.eval()

    loss_sum_t = zero_item_tensor(device)
    accuracy_top1_sum_t = zero_item_tensor(device)
    step_count = 0
    with torch.no_grad():
        progress = tqdm(enumerate(dataloader))
        for batch in progress:
            images, categories = batch[0].to(device, non_blocking=True), \
                                batch[1].to(device, non_blocking=True)

            prediction_logits = model(images)
            loss = criterion(prediction_logits, categories)
            num_categories = prediction_logits.size(1)
            loss_sum_t += loss
            accuracy_top1_sum_t += accuracy(prediction_logits, categories, topk=min(1, num_categories))
            avg_loss = loss_sum_t / (step_count + 1)
            avg_acc = accuracy_top1_sum_t / (step_count + 1)
            progress.set_description(
                'epoch: {}, loss: {:.6f}, acc: {:.4f}'
                    .format(epoch, avg_loss, avg_acc)
            )
            step_count += 1

    loss_avg = loss_sum_t.item() / step_count
    accuracy_top1_avg = accuracy_top1_sum_t.item() / step_count

    return loss_avg, accuracy_top1_avg



def print_msg(msg, appendixs=[]):
    max_len = len(max([msg, *appendixs], key=len))
    print('=' * max_len)
    print(msg)
    for appendix in appendixs:
        print(appendix)
    print('=' * max_len)

if __name__ == '__main__':
    conf_mat = np.array([
        [37, 8, 5, 0, 0],
        [8, 32, 8, 2, 0],
        [6, 6, 31, 5, 2],
        [1, 1, 5, 39, 4],
        [1, 1, 4, 10, 34]
    ])
    # print(quadratic_weighted_kappa(conf_mat))
    y_pred = torch.tensor(np.random.rand(16, 5))
    y = torch.tensor(np.random.randint(0, 5, (16)))
    print(accuracy(y_pred, y, 1))