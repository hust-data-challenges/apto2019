""" Post-process regularization steps such as gradient normalizing. """

from torch.nn.utils import clip_grad_norm_
import torch
import numpy as np

from .base import Callback


__all__ = ['GradientNormClipping', 'EarlyStopping']


class GradientNormClipping(Callback):
    """Clips gradient norm of a module's parameters.

    The norm is computed over all gradients together, as if they were
    concatenated into a single vector. Gradients are modified
    in-place.

    See :func:`torch.nn.utils.clip_grad_norm_` for more information.

    Parameters
    ----------
    gradient_clip_value : float (default=None)
      If not None, clip the norm of all model parameter gradients to this
      value. The type of the norm is determined by the
      ``gradient_clip_norm_type`` parameter and defaults to L2.

    gradient_clip_norm_type : float (default=2)
      Norm to use when gradient clipping is active. The default is
      to use L2-norm. Can be 'inf' for infinity norm.

    """
    def __init__(
            self,
            gradient_clip_value=None,
            gradient_clip_norm_type=2,
    ):
        self.gradient_clip_value = gradient_clip_value
        self.gradient_clip_norm_type = gradient_clip_norm_type

    def on_grad_computed(self, _, named_parameters, **kwargs):
        if self.gradient_clip_value is None:
            return

        clip_grad_norm_(
            (p for _, p in named_parameters),
            max_norm=self.gradient_clip_value,
            norm_type=self.gradient_clip_norm_type,
        )


class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience."""
    def __init__(self, patience=7, verbose=False):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
                            Default: 7
            verbose (bool): If True, prints a message for each validation loss improvement.
                            Default: False
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf

    def __call__(self, val_loss, model):

        score = -val_loss

        if self.best_score is None:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score:
            self.counter += 1
            print(f'EarlyStopping counter: {self.counter} out of {self.patience}')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print(f'Validation loss decreased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ...')
        torch.save(model.state_dict(), 'checkpoint.pt')
        self.val_loss_min = val_loss

if __name__ == '__main__':
    """
    early_stopping = EarlyStopping(patience=patience, verbose=True)
    for epoch in range(1, n_epochs + 1):
        model.train()
        train progress
        model.eval()
        evaluation progress
        -> train_loss, val_loss, avg_train_loss, avg_val_loss
        train_losses = []
        valid_losses = []
        
        # early_stopping needs the validation loss to check if it has decresed, 
        # and if it has, it will make a checkpoint of the current model
        early_stopping(valid_loss, model)
        
        if early_stopping.early_stop:
            print("Early stopping")
            break
    """