import torch
import numpy as np
from tqdm import tqdm
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from libs.utils.common import print_config
from libs.datasets.apto_dataset import APTODataset, StratifiedSampler, TrainDataProvider, \
    ScheduledWeightedSampler
from libs.utils.network import get_learning_rate
from libs.utils.common import create_if_need
from libs.datasets.transformers import transform_generate
from libs.models import create_model, create_backbone
from libs.utils.common import print_msg, zero_item_tensor
from libs.losses import FocalLoss, MSELoss, regularize_loss
from libs.optimizers import SGD
from libs.lr_scheduling.warmup_lr_scheduler import WarmupLRScheduler
from libs.modules import _eval, accuracy, quadratic_weighted_kappa, regre_accuracy
from libs.losses.ordinal_regression_loss import CumulativeLinkLoss
import time, datetime
import psutil
from tensorboardX import SummaryWriter

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def main(cfg, print_model=False):
    print("Argument: ")
    print_config(cfg)

    phase = cfg.phase
    base_data_dir = cfg.dataset.base_dir
    test_size = cfg.dataset.test_size
    output_dir = cfg.log_dir
    augmental_scale = [float(eval(i)) if isinstance(i, str) else i for i in cfg.augmentation.scale]
    augmental_stretch_ratio = cfg.augmentation.stretch_ratio
    augmental_rotation = cfg.augmentation.rotation
    augmental_translation_ratio = [float(eval(i)) if isinstance(i, str) else i
                                   for i in cfg.augmentation.translation_ratio]
    augmental_sigma = cfg.augmentation.sigma

    model_type = cfg.model.type
    image_size = cfg.model.image_size
    feature_dim = cfg.model.feature_dim
    num_categories = cfg.model.num_categories
    net_size = cfg.model.net_size

    exp_dir = cfg.exp_dir
    checkpoint = cfg.checkpoint

    batch_size = cfg.train.batch_size
    max_epoch = cfg.train.epochs
    num_workers = cfg.train.num_workers
    pin_memory = cfg.train.pin_memory == 'True'
    fold = cfg.train.fold
    confusion_set = cfg.train.confusion_set
    extra_loss = cfg.train.extra_loss == 'True'

    optimizer_type = cfg.train.optimizer.type
    learning_rate = eval(cfg.train.optimizer.lr)
    momentum = cfg.train.optimizer.momentum
    weight_decay = cfg.train.optimizer.weight_decay

    lr_scheduler = cfg.train.lr_scheduler.type
    milestones = cfg.train.lr_scheduler.milestones
    warmup_epoch = cfg.train.lr_scheduler.warm_up_epochs

    create_if_need("{}/logs/optim".format(output_dir))
    create_if_need("{}/logs/train".format(output_dir))
    create_if_need("{}/logs/val".format(output_dir))
    optim_summary_writer = SummaryWriter(log_dir="{}/logs/optim".format(output_dir))
    train_summary_writer = SummaryWriter(log_dir="{}/logs/train".format(output_dir))
    val_summary_writer = SummaryWriter(log_dir="{}/logs/val".format(output_dir))

    train_data_provider = TrainDataProvider(base_data_dir, test_size, fold, confusion_set, 'eval' not in phase)

    transform = transform_generate(image_size, augmental_scale, augmental_stretch_ratio,
                                     augmental_rotation, augmental_translation_ratio, augmental_sigma)

    train_set = APTODataset(train_data_provider.train_set_df, image_size, transform['train'])
    weighted_sampler = ScheduledWeightedSampler(len(train_set), train_set.categories, 0.975, True)
    # stratified_sampler = StratifiedSampler(train_data_provider.train_set_df['categories'], batch_size)
    train_data_loader = DataLoader(train_set, batch_size=batch_size, shuffle=False,
                            sampler=weighted_sampler, num_workers=num_workers, pin_memory=pin_memory)

    val_set = APTODataset(train_data_provider.val_set_df, image_size, transform['test'])
    val_data_loader = \
        DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=pin_memory)

    model = create_model(model_type, num_categories, net_size)
    if print_model:
        print(model)

    if checkpoint:
        pretrained_dict = model.load_weights(checkpoint, ['fc', 'dense'])
        print_msg('Loaded weights from {}: '.format(checkpoint), sorted(pretrained_dict.keys()))

    criterion = CumulativeLinkLoss()
    optimizer = SGD(model, learning_rate, momentum=0.9, nesterov=True, weight_decay=0.0005)

    warmup_batch = (len(train_data_loader) // batch_size) * warmup_epoch
    warmup_scheduler =  WarmupLRScheduler(optimizer, warmup_batch, learning_rate)

    #=========train===================
    max_acc = 0
    record_epochs, accs, losses = [], [], []
    batch_count = 0
    model.train()
    for epoch in range(max_epoch):
        epoch_start_time = time.time()

        print("memory used: {:.2f} GB".format(psutil.virtual_memory().used / 2 ** 30), flush=True)
        weighted_sampler.step()

        # learning rate update
        lr_scheduler.step()
        if epoch in milestones:
            curr_lr = optimizer.param_groups[0]['lr']
            print_msg('Learning rate decayed to {}'.format(curr_lr))
        if epoch > 1 and epoch <= warmup_epoch:
            curr_lr = optimizer.param_groups[0]['lr']
            print_msg('Learning rate warmup to {}'.format(curr_lr))

        train_loss = 0
        train_acc = 0
        val_loss = 0
        val_acc = 0
        correct = 0
        total = 0
        avg_loss = 0
        progress = tqdm(enumerate(train_data_loader))
        for step, train_data in progress:
            if epoch <= warmup_epoch:
                warmup_scheduler.step()

            X, y = train_data
            X, y = X.to(device), y.float().to(device)

            # forward
            y_pred = model(X)
            loss = criterion(y_pred, y)

            # backward
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # metrics
            train_loss += loss.item()
            total += y.size(0)
            correct += accuracy(y_pred, y, 1)
            avg_loss = train_loss / (step + 1)
            avg_acc = correct / total
            train_acc += avg_acc
            batch_count += 1
            optim_summary_writer.add_scalar("lr", get_learning_rate(optimizer), batch_count)
            progress.set_description(
                'train - epoch: {}, loss: {:.6f}, acc: {:.4f}'
                    .format(epoch, avg_loss, avg_acc)
            )
        model.eval()
        progress = tqdm(enumerate(val_data_loader))

        with torch.no_grad():
            for step, train_data in progress:
                X, y = train_data
                X, y = X.to(device), y.float().to(device)

                y_pred = model(X)
                eval_loss = criterion(y_pred, y)
                total += y.size(0)
                correct += accuracy(y_pred, y, 1)
                avg_loss = eval_loss / (step + 1)
                avg_acc = correct / total
                val_acc += avg_acc
                val_loss += eval_loss
                progress.set_description(
                    'eval - epoch: {}, loss: {:.6f}, acc: {:.4f}'
                        .format(epoch, avg_loss, avg_acc)
                )

        epoch_end_time = time.time()
        epoch_duration_time = epoch_end_time - epoch_start_time

        # save model
        # c_matrix = np.zeros((5, 5), dtype=int)
        # acc = _eval(model, val_set_data_loader, c_matrix)
        # kappa = quadratic_weighted_kappa(c_matrix)
        # print('validation accuracy: {}, kappa: {}'.format(acc, kappa))
        print("[%03d] %ds, lr: %.6f, loss: %.4f, val_loss: "
            "%.4f, acc: %.4f, val_acc: %.4f" %(
            epoch + 1, epoch_duration_time, get_learning_rate(optimizer),
            train_loss / len(train_data_loader), val_loss / len(val_data_loader),
             train_acc / len(train_data_loader), val_acc / len(val_data_loader)
        ))

        if val_acc > max_acc:
            torch.save(model, exp_dir)
            max_acc = val_acc
            print_msg('Model save at {}'.format(exp_dir))

        train_summary_writer.add_scalar("loss", train_loss, epoch + 1)
        train_summary_writer.add_scalar("acc", train_acc, epoch + 1)
        val_summary_writer.add_scalar("loss", train_loss, epoch + 1)
        val_summary_writer.add_scalar("acc", train_acc, epoch + 1)

    optim_summary_writer.close()
    train_summary_writer.close()
    val_summary_writer.close()

    return record_epochs, accs, losses


def regression_main(cfg, print_model=False):
    print("Argument: ")
    print_config(cfg)

    phase = cfg.phase
    base_data_dir = cfg.dataset.base_dir
    test_size = cfg.dataset.test_size
    output_dir = cfg.log_dir
    augmental_scale = [float(eval(i)) if isinstance(i, str) else i for i in cfg.augmentation.scale]
    augmental_stretch_ratio = cfg.augmentation.stretch_ratio
    augmental_rotation = cfg.augmentation.rotation
    augmental_translation_ratio = [float(eval(i)) if isinstance(i, str) else i
                                   for i in cfg.augmentation.translation_ratio]
    augmental_sigma = cfg.augmentation.sigma

    model_type = cfg.model.type
    image_size = cfg.model.image_size
    feature_dim = cfg.model.feature_dim
    num_categories = cfg.model.num_categories
    net_size = cfg.model.net_size

    exp_dir = cfg.exp_dir
    checkpoint = cfg.checkpoint

    batch_size = cfg.train.batch_size
    max_epoch = cfg.train.epochs
    num_workers = cfg.train.num_workers
    pin_memory = cfg.train.pin_memory == 'True'
    fold = cfg.train.fold
    confusion_set = cfg.train.confusion_set
    extra_loss = cfg.train.extra_loss == 'True'

    optimizer_type = cfg.train.optimizer.type
    learning_rate = cfg.train.optimizer.lr
    momentum = cfg.train.optimizer.momentum
    weight_decay = cfg.train.optimizer.weight_decay

    lr_scheduler = cfg.lr_scheduler.type
    milestones = cfg.lr_scheduler.milestones
    warmup_epoch = cfg.lr_scheduler.warm_up_epochs

    optim_summary_writer = SummaryWriter(log_dir="{}/logs/optim".format(output_dir))
    train_summary_writer = SummaryWriter(log_dir="{}/logs/train".format(output_dir))
    val_summary_writer = SummaryWriter(log_dir="{}/logs/val".format(output_dir))

    train_data_provider = TrainDataProvider(base_data_dir, test_size, fold, confusion_set, 'eval' not in phase)

    transform = transform_generate(image_size, augmental_scale, augmental_stretch_ratio,
                                     augmental_rotation, augmental_translation_ratio, augmental_sigma)

    train_set = APTODataset(train_data_provider.train_set_df, image_size, transform['train'])
    weighted_sampler = ScheduledWeightedSampler(len(train_set), train_set.categories, 0.975, True)
    # stratified_sampler = StratifiedSampler(train_data_provider.train_set_df['categories'], batch_size)
    train_data_loader = DataLoader(train_set, batch_size=batch_size, shuffle=False,
                            sampler=weighted_sampler, num_workers=num_workers, pin_memory=pin_memory)

    val_set = APTODataset(train_data_provider.val_set_df, image_size, transform['test'])
    val_data_loader = \
        DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=pin_memory)

    model = create_backbone(model_type, num_categories, net_size).to(device)
    if print_model:
        print(model)

    if checkpoint:
        pretrained_dict = model.load_weights(checkpoint, ['fc', 'dense'])
        print_msg('Loaded weights from {}: '.format(checkpoint), sorted(pretrained_dict.keys()))

    criterion = MSELoss()
    optimizer = SGD(model, learning_rate, momentum=0.9, nesterov=True, weight_decay=0.0005)

    lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=milestones, gamma=0.1)

    print_msg('Trainable layers: ', ['{}\t{}'.format(k, v) for k, v in model.layer_configs()])

    #=========train===================
    max_kappa = 0
    model.train()
    for epoch in range(max_epoch):
        # epoch_start_time = time.time()

        print("memory used: {:.2f} GB".format(psutil.virtual_memory().used / 2 ** 30), flush=True)
        if weighted_sampler:
            weighted_sampler.step()

        # learning rate update
        if lr_scheduler:
            lr_scheduler.step()
            if epoch in lr_scheduler.milestones:
                print_msg('Learning rate decayed to {}'.format(lr_scheduler.get_lr()[0]))


        train_loss_sum = zero_item_tensor(device)
        train_acc_sum = zero_item_tensor(device)
        epoch_batch_iter_count = 0
        correct = 0
        total = 0
        progress = tqdm(enumerate(train_data_loader))
        for step, train_data in progress:
            X, y = train_data
            X, y = X.to(device), y.float().to(device)

            # forward
            y_pred = model(X)
            loss = criterion(y_pred, y)
            if extra_loss:
                loss += regularize_loss(model, X, y_pred, y)

            # backward
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # metrics
            with torch.no_grad():
                total += y.size(0)
                correct += regre_accuracy(y_pred, y) * y.size(0)

                train_loss_sum += loss
                avg_loss = train_loss_sum.item() / (step + 1)

                avg_acc = correct / total
                train_acc_sum += correct / total

            epoch_batch_iter_count += 1
            progress.set_description(
                'epoch: {}, loss: {:.6f}, acc: {:.4f}'
                    .format(epoch + 1, avg_loss, avg_acc)
            )

        train_loss_avg = train_loss_sum.item() / epoch_batch_iter_count
        train_acc_avg = train_acc_sum.item() / epoch_batch_iter_count

        # save model
        c_matrix = np.zeros((5, 5), dtype=int)
        acc = _eval(model, val_data_loader, c_matrix)
        kappa = quadratic_weighted_kappa(c_matrix)
        print('validation accuracy: {}, kappa: {}'.format(acc, kappa))

        optim_summary_writer.add_scalar("lr", get_learning_rate(optimizer), epoch + 1)
        train_summary_writer.add_scalar("loss", train_loss_avg, epoch + 1)
        train_summary_writer.add_scalar("acc", train_acc_avg, epoch + 1)
        val_summary_writer.add_scalar("acc", acc, epoch + 1)
        val_summary_writer.add_scalar("kappa", kappa, epoch + 1)

        # acc_kappa_06-20-2019_0-6943_0-7212.pth
        if kappa > max_kappa:
            model_name = 'acc_kappa_' + str(datetime.datetime.now().strftime("%Y-%m-%d")) + '_' + \
                         str(acc).replace('.', '-') + '_' + str(kappa).replace('.', '-') + '.pth'
            torch.save(model, exp_dir + model_name)
            max_kappa = kappa
            print_msg('Model save at {}'.format(exp_dir + model_name))

    # evaluate(checkpoint, test_data_loader)