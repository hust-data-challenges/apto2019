from torch import optim


def SGD(model, learning_rate, momentum=0.9, nesterov=True, weight_decay=0.0005):
    return optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum,
                     nesterov=nesterov, weight_decay=weight_decay)

def Adam(model, learning_rate=1e-3):
    return optim.Adam(model.parameters(),lr=learning_rate)