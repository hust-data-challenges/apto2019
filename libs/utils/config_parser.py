class AttrDict(dict):

    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        elif name in self:
            return self[name]
        else:
            raise AttributeError(name)
            # pass

    def __setattr__(self, name, value):
        if name in self.__dict__:
            self.__dict__[name] = value
        else:
            self[name] = value


def config_recursion(cfg):
    if type(cfg) != dict:
        return cfg
    cfg = AttrDict(cfg)
    for k, v_ in cfg.items():
        cfg[k] = config_recursion(cfg[k])
    return cfg


def cfg_from_file(filename):
    """Load a config file and merge it into the default options."""
    import yaml
    with open(filename, 'r') as f:
        yaml_cfg = yaml.load(f, Loader=yaml.Loader)

    return config_recursion(yaml_cfg)