from sklearn.model_selection import StratifiedKFold
import cv2
from PIL import Image
import torch
import numpy as np
import os


def create_if_need(directory):
    os.makedirs(directory, exist_ok=True)


def read_lines(file_path):
    with open(file_path) as categories_file:
        return [l.rstrip("\n") for l in categories_file.readlines()]

def kfold_split(n_splits, values, classes):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    for train_value_indexes, test_value_indexes in skf.split(values, classes):
        train_values = [values[i] for i in train_value_indexes]
        test_values = [values[i] for i in test_value_indexes]
        yield train_values, test_values


def cv2_imread(path):
    img = cv2.imread(path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img

def pil_imread(path):
    img = Image.open(path)
    return img

def category_to_tensor(category):
    return torch.tensor(category).long()


def category_to_one_hot_tensor(category, num_categories):
    a = np.zeros((num_categories,), dtype=np.float32)
    a[category.item()] = 1.0
    return torch.tensor(a).float()

def print_msg(msg, appendixs=[]):
    max_len = len(max([msg, *appendixs], key=len))
    print('=' * max_len)
    print(msg)
    for appendix in appendixs:
        print(appendix)
    print('=' * max_len)

def print_config(cfg):
    for args in cfg.keys():
        print('[' + args + ']')
        # for arg in cfg[args]:
        #     print(arg)
        if hasattr(cfg[args], 'keys'):
            for key in cfg[args].keys():
                print('  ' + key + ': ' + str(cfg[args][key]))
        else:
            print('  ' + args + ': ' + str(cfg[args]))
        print('================================')

def read_confusion_set(file_path):
    with open(file_path) as categories_file:
        return [l.rstrip("\n") for l in categories_file.readlines()]

def zero_item_tensor(device):
    return torch.tensor(0.0).float().to(device, non_blocking=True)