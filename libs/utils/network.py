import numpy as np
import torch
import torch.nn as nn
from glob import glob


def image_to_tensor(image):
    if len(image.shape) == 2:
        image = np.expand_dims(image, 0)
    return torch.from_numpy(image / 255.).float()


def category_to_tensor(category):
    return torch.tensor(category.item()).long()


def category_to_one_hot_tensor(category, num_categories):
    a = np.zeros((num_categories,), dtype=np.float32)
    a[category.item()] = 1.0
    return torch.tensor(a).float()



def adjust_initial_learning_rate(optimizer, lr):
    for param_group in optimizer.param_groups:
        param_group["initial_lr"] = lr


def adjust_learning_rate(optimizer, lr):
    for param_group in optimizer.param_groups:
        param_group["lr"] = lr


def get_learning_rate(optimizer):
    for param_group in optimizer.param_groups:
        return param_group["lr"]


def with_he_normal_weights(layer):
    nn.init.kaiming_normal_(layer.weight, a=0, mode="fan_in")
    return layer


def freeze(model):
    for param in model.parameters():
        param.requires_grad = False


def unfreeze(model):
    for param in model.parameters():
        param.requires_grad = True


def zero_item_tensor(device):
    return torch.tensor(0.0).float().to(device, non_blocking=True)


def get_loss_target(criterion, categories, categories_one_hot):
    # if isinstance(criterion, SoftCrossEntropyLoss) or isinstance(criterion, SoftBootstrapingLoss):
    #     return categories_one_hot
    # else:
    #     return categories
    return categories


def check_model_improved(old_score, new_score, threshold=1e-4):
    return new_score - old_score > threshold


def find_sorted_model_files(base_dir):
    return sorted(glob("{}/model-*.pth".format(base_dir)), key=lambda e: int(os.path.basename(e)[6:-4]))




