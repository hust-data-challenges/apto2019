import torch
import numpy as np
from tqdm import tqdm
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from libs.utils.common import print_config
from libs.datasets.apto_dataset import APTODataset, StratifiedSampler, TrainDataProvider, \
    ScheduledWeightedSampler
from libs.utils.network import get_learning_rate
from libs.utils.common import create_if_need
from libs.datasets.transformers import transform_generate
from libs.models import create_model, create_backbone
from libs.utils.common import print_msg, zero_item_tensor
from libs.losses import FocalLoss, MSELoss, regularize_loss
from libs.optimizers import SGD
from libs.lr_scheduling.warmup_lr_scheduler import WarmupLRScheduler
from libs.modules import _eval, evaluate, accuracy, quadratic_weighted_kappa, regre_accuracy
from libs.losses.ordinal_regression_loss import CumulativeLinkLoss
import time, datetime
import psutil
from libs.lr_scheduling import CyclicCosAnnealingLR
from tensorboardX import SummaryWriter

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def main(cfg, print_model=False):
    print("Argument: ")
    print_config(cfg)

    phase = cfg.phase
    base_data_dir = cfg.dataset.base_dir
    test_size = cfg.dataset.test_size
    output_dir = cfg.log_dir
    augmental_scale = [float(eval(i)) if isinstance(i, str) else i for i in cfg.augmentation.scale]
    augmental_stretch_ratio = cfg.augmentation.stretch_ratio
    augmental_rotation = cfg.augmentation.rotation
    augmental_translation_ratio = [float(eval(i)) if isinstance(i, str) else i
                                   for i in cfg.augmentation.translation_ratio]
    augmental_sigma = cfg.augmentation.sigma

    model_type = cfg.model.type
    image_size = cfg.model.image_size
    feature_dim = cfg.model.feature_dim
    num_categories = cfg.model.num_categories
    net_size = cfg.model.net_size

    exp_dir = cfg.exp_dir
    checkpoint = cfg.checkpoint

    batch_size = cfg.train.batch_size
    max_epoch = cfg.train.epochs
    num_workers = cfg.train.num_workers
    pin_memory = cfg.train.pin_memory == 'True'
    fold = cfg.train.fold
    confusion_set = cfg.train.confusion_set
    extra_loss = cfg.train.extra_loss == 'True'

    optimizer_type = cfg.train.optimizer.type
    learning_rate = cfg.train.optimizer.lr
    momentum = cfg.train.optimizer.momentum
    weight_decay = cfg.train.optimizer.weight_decay

    lr_scheduler_type = cfg.train.lr_scheduler.type
    milestones = cfg.train.lr_scheduler.milestones
    warmup_epoch = cfg.train.lr_scheduler.warm_up_epochs

    # "{}/logs/optim".format(output_dir)
    optim_summary_writer = SummaryWriter(log_dir="{}/logs/optim".format(output_dir))
    train_summary_writer = SummaryWriter(log_dir="{}/logs/train".format(output_dir))
    val_summary_writer = SummaryWriter(log_dir="{}/logs/val".format(output_dir))

    train_data_provider = TrainDataProvider(base_data_dir, test_size, fold, confusion_set, 'eval' not in phase)

    transform = transform_generate(image_size, augmental_scale, augmental_stretch_ratio,
                                   augmental_rotation, augmental_translation_ratio, augmental_sigma)

    train_set = APTODataset(train_data_provider.train_set_df, image_size, transform['train'])
    weighted_sampler = ScheduledWeightedSampler(len(train_set), train_set.categories, 0.975, True)
    # stratified_sampler = StratifiedSampler(train_data_provider.train_set_df['categories'], batch_size)
    train_data_loader = DataLoader(train_set, batch_size=batch_size, shuffle=False,
                                   sampler=weighted_sampler, num_workers=num_workers, pin_memory=pin_memory)

    val_set = APTODataset(train_data_provider.val_set_df, image_size, transform['test'])
    val_data_loader = \
        DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=pin_memory)

    # model = create_backbone(model_type, num_categories, net_size).to(device)
    model = create_model(cfg.model.type, 5, 'ordinal_log', 'ordered', cfg.model.net_size).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = SGD(model, learning_rate, momentum=0.9, nesterov=True, weight_decay=0.0005)
    lr_scheduler = CyclicCosAnnealingLR(optimizer, milestones=[150, 220])

    if print_model:
        print(model)

    avg_acc, avg_loss = [], []
    train_loss = zero_item_tensor(device)
    train_acc = zero_item_tensor(device)
    model.train()
    for epoch in range(0, max_epoch):
        epoch_start_time = time.time()
        # print("memory used: {:.2f} GB".format(psutil.virtual_memory().used / 2 ** 30), flush=True)
        if weighted_sampler:
            weighted_sampler.step()

        if lr_scheduler:
            lr_scheduler.step()

        epoch_loss = 0
        correct = 0
        total = 0
        progress = tqdm(enumerate(train_data_loader))
        for step, train_data in progress:
            X, y = train_data
            X, y = X.cuda(), y.long().cuda()
            # forward
            y_pred = model(X)
            loss = criterion(y_pred, y)
            loss.backward()
            optimizer.step()

            # metrics
            epoch_loss += loss.item()
            epoch_acc = accuracy(y_pred, y) * y.size(0)
            total += y.size(0)
            correct += epoch_acc
            avg_loss = epoch_loss / (step + 1)
            avg_acc = correct / total
            train_loss += loss
            train_acc += epoch
            progress.set_description(
                'epoch: {}, loss: {:.6f}, acc: {:.4f}'
                    .format(epoch, avg_loss, avg_acc)
            )

        #eval
        val_loss, val_acc = evaluate(epoch, model, val_data_loader, criterion, 1)

        optim_summary_writer.add_scalar("lr", get_learning_rate(optimizer), epoch + 1)
        train_summary_writer.add_scalar("loss", train_loss, epoch + 1)
        train_summary_writer.add_scalar("acc", train_acc, epoch + 1)
        val_summary_writer.add_scalar("loss", val_loss, epoch + 1)
        val_summary_writer.add_scalar("acc", val_acc, epoch + 1)

        epoch_end_time = time.time()
        epoch_duration_time = epoch_end_time - epoch_start_time

        print("[%03d] %ds, lr: %.6f, loss: %.4f, val_loss: %.4f, acc: %.4f, val_acc: %.4f, ckpt: %d" % (
                epoch + 1,
                epoch_duration_time,
                get_learning_rate(optimizer),
                train_loss,
                val_loss,
                train_acc,
                val_acc,
                int(checkpoint)))

    optim_summary_writer.close()
    train_summary_writer.close()
    val_summary_writer.close()


if __name__ == '__main__':
    from libs.utils.config_parser import cfg_from_file
    cfg = cfg_from_file('./configs/resnet_train.yml')

    main(cfg, True)
    pass