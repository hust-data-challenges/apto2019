#!/usr/bin/env bash

pip install kaggle
pip install tensorboardX
apt install pv

echo '{"username":"tuanhleo11","key":"ac260b1174ba55811213f57f91689e99"}' > kaggle.json
mkdir -p ~/.kaggle/
cp kaggle.json ~/.kaggle/
chmode 600 ~/.kaggle/kaggle.json
cat ~/.kaggle/kaggle.json

if [ ! -d "./storage" ]
then
   mkdir storage
   echo "create folder storage"
fi

if [ ! -d "./storage/apto2019" ]
then
   mkdir storage/apto2019
   echo "create folder storage/apto2019"
fi

cd ./storage/apto2019

kaggle competitions download -c aptos2019-blindness-detection
mkdir train_images
mkdir test_images
unzip -o train_images.zip -d train_images |  pv -l >/dev/null
unzip -o test_images.zip -d test_images |  pv -l >/dev/null

cd ../..