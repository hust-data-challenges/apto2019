from libs.utils.config_parser import cfg_from_file
from libs.utils.common import print_config, print_msg
from libs.models import o_ONet, create_model, create_infer_model
import numpy as np
import torch

if __name__ == "__main__":
    cfg = cfg_from_file('./configs/small_o_ONet_train.yml')
    # print_config(cfg)
    model = create_model('res_o_O', 5, net_size='small')
    # model = create_infer_model()
    # print_msg('Trainable layers: ', ['{}\t{}'.format(k, v) for k, v in model.layer_configs()])
    print(model.dim_fc_features)
    x = torch.FloatTensor(np.random.rand(2, 3, 112, 112))
    print(model(x).size())
